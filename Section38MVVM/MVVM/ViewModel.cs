﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Section38MVVM.MVVM
{
    public class ViewModel
    {
        private List<Model> models = new List<Model>();
        public ViewModel()
        {            
            models.Add(new Model("Hii Vishnu"));
        }
        public List<Model> Model
        {
            get { return models; }
            set { models = value; }
        }
    }
}
