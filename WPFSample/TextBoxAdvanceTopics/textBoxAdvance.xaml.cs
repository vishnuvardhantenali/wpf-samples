﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFSample.TextBoxAdvanceTopics
{
    /// <summary>
    /// Interaction logic for textBoxAdvance.xaml
    /// </summary>
    public partial class textBoxAdvance : Window
    {
        public textBoxAdvance()
        {
            InitializeComponent();
            txtMainTextBox.FontFamily = new FontFamily("consolas");
        }

        private static bool wordWrappingFlag;
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }
        
        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.FontSize = txtMainTextBox.FontSize + 2;
            //txtMainTextBox.FontSize = 50;
        }

        private void WordWrapper_Click(object sender, RoutedEventArgs e)
        {
            if (!wordWrappingFlag)
            {
                txtMainTextBox.TextWrapping = TextWrapping.Wrap;
                wordWrappingFlag = true;
            }
            else
            {
                txtMainTextBox.TextWrapping = TextWrapping.NoWrap;
                wordWrappingFlag = false;
            }
        }

        private void BtnZoomOut_Click(object sender, RoutedEventArgs e)
        {
            if (txtMainTextBox.FontSize >= 10)
            {
                txtMainTextBox.FontSize = txtMainTextBox.FontSize-2;
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.SelectAll();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void CopyContent_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Copy();
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Undo();
        }

        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Redo();
        }

        private void FontSelection_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.FontFamily = new FontFamily("consolas");
            //txtMainTextBox.FontFamily = new FontFamily("Elephant");
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Text = string.Empty;
        }

        private void TxtMainTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //handler codes go here as needed.
            if (e.Key == Key.Escape)
            {
                //Close();
                //do something if 'enter' key is pressed.
            }
        }
    }
}
