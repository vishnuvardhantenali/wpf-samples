﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Section21TextboxAdvancedProperties
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void btnZoomIn_Click(object sender, RoutedEventArgs e)
        {
            txtWorkspace.FontSize = txtWorkspace.FontSize + 2;
        }

        private void btnZoomOut_Click(object sender, RoutedEventArgs e)
        {
            if (txtWorkspace.FontSize <= 3)
            {
                return;
            }
            txtWorkspace.FontSize = txtWorkspace.FontSize - 2;
        }

        private void btnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            txtWorkspace.Focus();
            txtWorkspace.SelectAll();            
        }

        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            txtWorkspace.Undo();
        }

        private void btnRedo_Click(object sender, RoutedEventArgs e)
        {
            txtWorkspace.Redo();
        }

        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            txtWorkspace.Focus();
            txtWorkspace.Copy();
        }

        private void btnPaste_Click(object sender, RoutedEventArgs e)
        {
            txtWorkspace.Paste();
        }

        private void btnCut_Click(object sender, RoutedEventArgs e)
        {
            txtWorkspace.Cut();
        }
    }
}
