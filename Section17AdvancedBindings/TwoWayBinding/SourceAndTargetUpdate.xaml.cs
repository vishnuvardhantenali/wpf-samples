﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section17AdvancedBindings.TwoWayBinding
{
    /// <summary>
    /// Interaction logic for SourceAndTargetUpdate.xaml
    /// </summary>
    public partial class SourceAndTargetUpdate : Window
    {
        public SourceAndTargetUpdate()
        {
            InitializeComponent();
        }

        private void TxtCntrl_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            MessageBox.Show("Source Updated");
        }

        private void TxtCntrl_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            MessageBox.Show("Target Updated");
        }
    }
}
