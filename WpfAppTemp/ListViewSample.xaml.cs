﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppTemp
{
    /// <summary>
    /// Interaction logic for ListViewSample.xaml
    /// </summary>
    public partial class ListViewSample : Window
    {
        public ListViewSample()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            listViewMain.Items.Add(txtData.Text);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            listViewMain.Items.Remove(listViewMain.SelectedItem);
        }

        private void btnDeleteAll_Click(object sender, RoutedEventArgs e)
        {
            listViewMain.Items.Clear();
        }

        private void btnAddListViewItems_Click(object sender, RoutedEventArgs e)
        {
            List<string> items = new List<string>
            {
                "Man",
                "Woman",
                "Boy",
                "Girl"
            };

            lvSecond.ItemsSource = items;
        }

        private void btnLoadImages_Click(object sender, RoutedEventArgs e)
        {
            ListViewItem listViewItem = new ListViewItem();
            Image image = new Image();
            Label lblImage = new Label();
            StackPanel stackPanel = new StackPanel();

            lblImage.Content = "Sample Image";
            image.Source = new BitmapImage(new Uri(@"C:\Users\Admin\source\repos\WPFSample\WpfAppTemp\Icons\copy.png"));
            image.Width = 50;
            image.Height = 50;
            
            stackPanel.Orientation=Orientation.Horizontal;
            stackPanel.Children.Add(image);
            stackPanel.Children.Add(lblImage);

            listViewItem.Content = stackPanel;
            lvImageViewbyCode.Items.Add(listViewItem);
        }

        private void btnMultiColumn_Click(object sender, RoutedEventArgs e)
        {
            MultiColumn multiColumn = new MultiColumn("Vishnu", " Vardhan", 25);
            lvMultiImageView.Items.Add(multiColumn);
            
        }
    }
    public class MultiColumn
    {
        public string FirstName { get; set; }
        public string LastName { get; set; } 
        public int Age{ get; set; }
        public MultiColumn(string fname, string lname, int age)
        {
            FirstName = fname;
            LastName = lname;
            Age = age;
        }
    }
}
