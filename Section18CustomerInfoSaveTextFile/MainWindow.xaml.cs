﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Shapes;

namespace Section18CustomerInfoSaveTextFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string path1 = Environment.CurrentDirectory + $"\\Customer\\";
            string path2 = Environment.CurrentDirectory + $"\\Customer\\";
            if (!Directory.Exists(path1))
            {
                Directory.CreateDirectory(path1);
            }
            if (!Directory.Exists(path2))
            {
                Directory.CreateDirectory(path2);
            }

            if (string.IsNullOrEmpty(txtBoxId.Text) || string.IsNullOrWhiteSpace(txtBoxId.Text))
            {
                MessageBox.Show("Please Enter the ID");
                return;
            }
            if (string.IsNullOrEmpty(txtBoxFName.Text) || string.IsNullOrWhiteSpace(txtBoxFName.Text))
            {
                MessageBox.Show("Please Enter the First Name");
                return;
            }
            if (string.IsNullOrEmpty(txtBoxLName.Text) || string.IsNullOrWhiteSpace(txtBoxLName.Text))
            {
                MessageBox.Show("Please Enter the Last Name");
                return;
            }
            File.WriteAllText(path1+$"{txtBoxId.Text}_FirstName.log",txtBoxFName.Text);
            File.WriteAllText(path2+$"{txtBoxId.Text}_LastName.log",txtBoxLName.Text);
           
            MessageBox.Show("Save");
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            txtBoxId.Text = "";
            txtBoxFName.Text = "";
            txtBoxLName.Text = "";
            txtBoxId.Focus();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string path1 = Environment.CurrentDirectory + $"\\Customer\\";
                string path2 = Environment.CurrentDirectory + $"\\Customer\\";

                if (string.IsNullOrEmpty(txtBoxSearchID.Text) || string.IsNullOrWhiteSpace(txtBoxSearchID.Text))
                {
                    MessageBox.Show("Please Search by ID");
                    return;
                }

                txtBoxId.Text = txtBoxSearchID.Text;
                txtBoxFName.Text = File.ReadAllText(path1 + $"{txtBoxSearchID.Text}_FirstName.log");
                txtBoxLName.Text = File.ReadAllText(path2 + $"{txtBoxSearchID.Text}_LastName.log");
            }
            catch
            {
                txtBoxId.Text = "";
                txtBoxSearchID.Focus();
                MessageBox.Show($"Record are not created with {txtBoxSearchID.Text}");
            }
        }
    }
}
