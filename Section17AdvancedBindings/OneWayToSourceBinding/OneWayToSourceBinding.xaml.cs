﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section17AdvancedBindings.OneWayToSourceBinding
{
    /// <summary>
    /// Interaction logic for OneWayToSourceBinding.xaml
    /// </summary>
    public partial class OneWayToSourceBinding : Window
    {
        public OneWayToSourceBinding()
        {
            InitializeComponent();
        }

        private void TxtCtrl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Convert.ToInt32(txtCtrl.Text) >= 30000) 
            {
                MessageBox.Show("Please select Below 30,000 only");
            }
        }
    }
}
