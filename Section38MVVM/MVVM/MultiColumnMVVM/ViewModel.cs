﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Section38MVVM.MVVM.MultiColumnMVVM
{
    class ViewModel
    {
        private List<Model> models = new List<Model>();
        public void mMultiViewModel()
        {
            string imageFile = @"C:\Users\tvishnuvardhan\Downloads\Hopstarter-Sleek-Xp-Basic-Administrator.ico";
            BitmapImage bitmap = new BitmapImage(new Uri(imageFile));
            models.Add(new Model(1,bitmap, "VISHNU", "VARDHAN", DateTime.Now));
        }
        public List<Model> Models
        {
            get { return models; }
            set
            {
                models = value;
            }
        }
    }
}
