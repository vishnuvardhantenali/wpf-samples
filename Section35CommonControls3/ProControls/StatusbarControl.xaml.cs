﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section35CommonControls3.ProControls
{
    /// <summary>
    /// Interaction logic for StatusbarControl.xaml
    /// </summary>
    public partial class StatusbarControl : Window
    {
        public StatusbarControl()
        {
            InitializeComponent();
            SliderZoom.Value = 100;
        }
        private void BtnZoomIn_Click(object sender, RoutedEventArgs e)
        {
            if (txtMainTextBox.FontSize <= 100)
                txtMainTextBox.FontSize = txtMainTextBox.FontSize + 2;
        }

        private void BtnZoomOut_Click(object sender, RoutedEventArgs e)
        {
            if (txtMainTextBox.FontSize > 10)
            {
                txtMainTextBox.FontSize = txtMainTextBox.FontSize - 2;
            }
        }

        private void WrapCheck_Checked(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.TextWrapping = TextWrapping.Wrap;
        }

        private void WrapCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.TextWrapping = TextWrapping.NoWrap;
        }

        private void File_Open_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Open");
        }

        private void MainWindowApp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                Help_MenuItem_Click(sender, e);
            }
        }

        private void Help_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            //VersionWindow version = new VersionWindow();
            //version.ShowDialog();

        }

        private void View_Zoom_RestoreDefaultZoom_Button_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.FontSize = 12;
        }

        private void File_Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void File_New_Button_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Text = string.Empty;
        }

        private void File_Save_Button_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.DefaultExt = ".txt";
            save.Filter = "Text documents (.txt)|*.txt";
            if (save.ShowDialog() == true)
            {
                string filename = save.FileName;
                if (!Directory.Exists(filename))
                {
                    StreamWriter stream = new StreamWriter(filename);
                    stream.WriteLine(txtMainTextBox.Text);
                    stream.Close();
                }
            }
        }

        private void File_Open_Button_Click_2(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".txt";
            openFileDialog.Filter = "Text documents (.txt)|*.txt";
            if (openFileDialog.ShowDialog() == true)
                txtMainTextBox.Text = File.ReadAllText(openFileDialog.FileName);
        }

        private void Edit_Copy_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Copy();
        }

        private void Edit_Cut_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Cut();
        }

        private void Edit_Paste_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Paste();
        }

        private void Edit_Delete_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Text = "";
        }

        private void Edit_Undo_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            txtMainTextBox.Undo();
        }

        private void TxtMainTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.lblCount.Content = this.txtMainTextBox.Text.Length;
        }

        private void BtnCustom_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".txt";
            openFileDialog.Filter = "Text documents (.txt)|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                string[] t = File.ReadAllLines(openFileDialog.FileName);
                StreamWriter writer = new StreamWriter("E:\\Font.txt");
                foreach (var y in t)
                {
                    string rr = "<ComboBoxItem Content=\"" + y;
                    string tt = " HorizontalAlignment =\"Left\" Width=\"{ DynamicResource cmbWidth}\" FontSize=\"{ DynamicResource cmbZoom}\"/>";
                    //writer.WriteLine(rr + "\"" + tt);
                    //writer.WriteLine(rr);
                    //writer.WriteLine($" <ComboBoxItem Content="+""+y+""+" HorizontalAlignment=\"Left\" Width=\"{ DynamicResource cmbWidth}\" FontSize=\"{ DynamicResource cmbZoom}\"/>");
                    writer.WriteLine($" < MenuItem Header=" + y + " IsCheckable=\"True\" Click=\"Edit_Font_Click\"/>");
                }
                writer.Close();
                MessageBox.Show("Conversion comepleted");
            }
        }

        private void CmbFontSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void FontMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuitem = (MenuItem)sender;
            string FontMenuItem = menuitem.Header.ToString();

            FontFamily font = new FontFamily(FontMenuItem);
            txtMainTextBox.FontFamily = font;
        }

        private void MenuItem_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MenuItem menuitem = (MenuItem)sender;
            string FontMenuItem = menuitem.Header.ToString();

            FontFamily font = new FontFamily(FontMenuItem);
            txtMainTextBox.FontFamily = font;
        }

        private void SliderZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void ZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            SliderZoom.Value = SliderZoom.Value - 10;
        }

        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            SliderZoom.Value = SliderZoom.Value + 10;
        }
    }
}
