﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Section38MVVM.MVVM.MultiColumnMVVM
{
    public class Model
    {
        public int SNo { get; set; }
        public BitmapImage Image { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Date { get; set; }

        public Model(int no, BitmapImage image, string fname, string lname, DateTime date)
        {
            SNo = no;
            Image = image;
            FirstName = fname;
            LastName = lname;
            Date = date;           
        }
    }
}
