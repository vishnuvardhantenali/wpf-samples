﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfResourcesExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnWindowLoad_Click(object sender, RoutedEventArgs e)
        {
            //getting the Window Resource value
            object ob=Resources["btnWidth"];
            lblWindowLoad.Content = ob;

            //setting the Window Resource value
            Resources["btnWidth"] = (Double)100;
            
        }

        private void BtnGridLoad_Click(object sender, RoutedEventArgs e)
        {
            object ob1 = SubGrid1.Resources["SubGrid1Width"];
            lblGridLoad.Content = ob1;
        }

        private void BtnAppLoad_Click(object sender, RoutedEventArgs e)
        {
            object ob2 = Application.Current.Resources["btnLogInHeight"];
            lblAppLoad.Content = (ob2);
        }
    }
}
