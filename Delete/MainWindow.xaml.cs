﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;

namespace Delete
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Process[] processes = Process.GetProcessesByName("notepad");
            if (processes.Length == 0)
            {
                btnDelete.IsEnabled = true;

                //MessageBox.Show("not running");
            }
            else
            {
                btnDelete.IsEnabled = false;
                //MessageBox.Show("running");
            }
            if (btnDelete.IsEnabled)
            {
                System.Diagnostics.Process.Start("notepad");
            }
            Thread.Sleep(100);
            btnDelete.IsEnabled = true;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show("Key Down");
            Process[] processes = Process.GetProcessesByName("notepad");
            if (processes.Length == 0)
            {
                btnDelete.IsEnabled = false;

                //MessageBox.Show("not running");
            }
            else
            {
                btnDelete.IsEnabled = true;
                //MessageBox.Show("running");
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void BtnDelete_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("Key Down");
            Process[] processes = Process.GetProcessesByName("notepad");
            if (processes.Length == 0)
            {
                btnDelete.IsEnabled = true;

                //MessageBox.Show("not running");
            }
            else
            {
                btnDelete.IsEnabled = false;
                //MessageBox.Show("running");
            }

        }

        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            if (!btnDelete.IsEnabled)
            {
                //MessageBox.Show("Key Down");
                Process[] processes = Process.GetProcessesByName("notepad");
                if (processes.Length == 0)
                {
                    btnDelete.IsEnabled = true;

                    //MessageBox.Show("not running");
                }
                else
                {
                    btnDelete.IsEnabled = false;
                    //MessageBox.Show("running");
                }
            }
        }
    }
}
