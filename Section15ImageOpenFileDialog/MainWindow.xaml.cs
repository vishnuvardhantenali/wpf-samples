﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace Section15ImageOpenFileDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public Uri UriSource;
        int IsRotate = -1;

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            if (imgControl.Source != null)
            {
                IsRotate = -1;
                imgControl.Source = null;
            }
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.RestoreDirectory = true;
            dlg.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.gif|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                string selectedFileName = dlg.FileName;
                txtFilePath.Text = selectedFileName;
                UriSource = new Uri(selectedFileName);

                imgControl.Source = new BitmapImage(UriSource);
            }            
        }        
        private void btnRotate_Click(object sender, RoutedEventArgs e)
        {
            if (txtFilePath.Text == "")
            {
                MessageBox.Show("Please select the image");
                return;
            }
            BitmapImage bi = new BitmapImage();
            
            bi.BeginInit();
            bi.UriSource = UriSource;
            //Set image rotation
            switch (++IsRotate)
            {
                case 0:                    
                    bi.Rotation = Rotation.Rotate90;
                    break;
                case 1:                    
                    bi.Rotation = Rotation.Rotate180;
                    break;
                case 2:
                    bi.Rotation = Rotation.Rotate270;
                    break;
                case 3:                    
                    bi.Rotation = Rotation.Rotate0;                    
                    IsRotate = -1;
                    break;
            }           

            bi.EndInit();            
            imgControl.Source = bi;
        }
    }
}
